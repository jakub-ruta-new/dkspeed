<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 *
 *
 * @author jakub
 */
class seeder_202109222205_addRights extends \JR\CORE\database\migrations\Migrations {

//put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema) {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->startMigration(get_class($this));
        $dataKeys = array("right_name", "right_action",
            "right_description",
            "right_granteable_by", "visible_from");
        $multiInsertData = array(
            array("rounds", "view",
                "Right allow to see planned rounds", 4, 1),
            array("rounds", "edit",
                "Right allow to edit rounds", 4, 1),
            array("slots", "view",
                "See slots in rounds", 4, 1),
            array("slots", "all",
                "Right allow to open all slots", 4, 1)
        );
        $Schema->getDB()->insertMulti("rights", $multiInsertData, $dataKeys);
        $Schema->finishMigration(get_class($this));
    }

}
