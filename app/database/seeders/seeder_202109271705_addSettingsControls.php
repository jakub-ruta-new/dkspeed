<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 *
 *
 * @author jakub
 */
class seeder_202109271705_addSettingsControls extends \JR\CORE\database\migrations\Migrations {

//put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema) {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->startMigration(get_class($this));
        $dataKeys = array("id", "internal_name", "name", "description", "type", "active", "default", "min", "max", "options", "order_id", "hidden", "class");
        $multiInsertData = array([1, 'speed', 'Speed', 'Speed of round', 'numberbox', 1, '400', 1, 400, NULL, 10, 0, NULL],
            [2, 'unit_speed', 'Troops speed', 'Speed of units', 'numberbox', 1, '1.0', 0.1, 2.9, NULL, 20, 0, NULL],
            [3, 'moral', 'Moral', '', 'selectbox', 1, '0', NULL, NULL, '0:No morale;1:Points based;2:Points   time based;3:Points   unlimited time based', 30, 0, NULL],
            [6, 'attack_gap', 'Attack gap', 'Gap between two attacks from same village', 'numberbox', 1, '100', 30, 1000, NULL, 50, 0, NULL],
            [7, 'beginner_protection', 'Beginner protection', '[minutes]', 'numberbox', 1, '5', 0, 10000, NULL, 60, 0, NULL],
            [9, 'paladin', 'Paladin', '', 'selectbox', 1, '1', NULL, NULL, '0:Disabled;1:Enabled, one paladin per player;3:Enabled, multiple paladins, with skills', 80, 0, NULL],
            [10, 'archers', 'Archers', '', 'checkbox', 1, '0', NULL, NULL, NULL, 90, 0, NULL],
            [11, 'tech', 'Research System', '', 'selectbox', 1, '2', NULL, NULL, '0:10 levels;1:3 level [up to 15]; 2:Simple', 100, 0, NULL],
            [12, 'farm_limit', 'Farm limit', '0 - means no Farm limit (on bash round value is about 150)', 'numberbox', 1, '0', 0, 5000, NULL, 110, 0, NULL],
            [13, 'church', 'Church', '', 'checkbox', 1, '0', NULL, NULL, NULL, 120, 0, NULL],
            [14, 'watchtower', 'Watchtower', '', 'checkbox', 1, '0', NULL, NULL, NULL, 130, 0, NULL],
            [16, 'fake_limit', 'Fake limit', '0 - mean no fake limit', 'numberbox', 1, '0', 0, 10, NULL, 150, 0, NULL],
            [17, 'barbarian_rise', 'Barbarian rise', 'How fast barbs will growth (0 - no growth, 0,5 normal, 0,1 slow, 3 very fast)', 'numberbox', 1, '0.5', 0, 10, NULL, 160, 0, NULL],
            [18, 'barbarian_max_points', 'Barbarian growth up to:', '', 'numberbox', 1, '3000', 0, 13000, NULL, 170, 0, NULL],
            [19, 'hauls', 'Hauls (plunder villages for resources)', 'If you set limited, add how to description!', 'selectbox', 1, '1', NULL, NULL, '0:Off;1:On;2:Limited;3:On, with scavenging', 180, 0, NULL],
            [20, 'production', 'Production', '[%] Will be rounded to nearest possible value! (100 -> 100 % production (x1)', 'numberbox', 1, '100', 10, 600, NULL, 190, 0, NULL],
            [23, 'militia', 'Militia', '', 'checkbox', 1, '0', NULL, NULL, NULL, 220, 0, NULL],
            [24, 'snob', 'Snob', '', 'selectbox', 1, '0', NULL, NULL, '0:Coins;1:Coins 1/2;2:Coins 1/4;3:Coins 1/10;4:Packet system;5:The building costs per nobleman are constant;6:The first 3 nobleman have constant building costs, the 4th costs 4 times as many resources, the 5th 5 times;7:The building costs increase after the 2nd nobleman built;8:No', 230, 0, NULL],
            [25, 'no_barb_conquer', 'Prevent noble barbars', 'Include bonus villages', 'checkbox', 1, '0', NULL, NULL, NULL, 240, 0, NULL],
            [26, 'ally_members', 'Ally limit', '', 'numberbox', 1, '5', 1, 40, NULL, 240, 0, NULL],
            [27, 'ally_support', 'Send support outside tribe', '', 'selectbox', 1, '0', NULL, NULL, '0:Can;1:Can not', 250, 0, NULL],
            [59, 'ally_support_type', 'Send support type', 'When system check if support can be send', 'selectbox', 1, '0', NULL, NULL, '0:Sending;1:Arrival', 255, 0, NULL],
            [28, 'ally_no_leave', 'No leave tribe', '', 'checkbox', 1, '0', NULL, NULL, NULL, 260, 0, NULL],
            [29, 'func', 'Distance between villages', '', 'selectbox', 1, '2', NULL, NULL, '2:Normal;3:Reduced;4:Increased', 270, 0, NULL],
            [30, 'bonus_villages', 'Bonus villages', 'amout per player that will be spawn on player registration', 'numberbox', 1, '0', 0, 20, NULL, 280, 0, NULL],
            [31, 'barbar_villages', 'Barbarian villages', 'amout per player that will be spawn on player registration', 'numberbox', 1, '0', 0, 20, NULL, 290, 0, NULL],
            [32, 'start_villages', 'Start villages', 'How many villages players start with', 'numberbox', 1, '1', 1, 10, NULL, 300, 0, NULL],
            [33, 'mood_min', 'Minimum loss of loyality with nobleman', '', 'numberbox', 1, '20', 0, 100, NULL, 310, 0, NULL],
            [34, 'mood_max', 'Maximum loss of loyality with nobleman', '', 'numberbox', 1, '35', 0, 100, NULL, 311, 0, NULL],
            [36, 'ally_harm', 'No harm ally members', 'Prevent attack to same tribe member', 'checkbox', 1, '0', NULL, NULL, NULL, 265, 0, NULL],
            [39, 'custom_main', 'Custom main', '', 'numberbox', 1, '1', 1, 50, NULL, 201, 0, 'toogled'],
            [40, 'custom_farm', 'Custom farm', '', 'numberbox', 1, '1', 1, 50, NULL, 202, 0, 'toogled'],
            [41, 'custom_storage', 'Custom storage', '', 'numberbox', 1, '1', 1, 50, NULL, 203, 0, 'toogled'],
            [42, 'custom_place', 'Custom place', '', 'numberbox', 1, '0', 0, 2, NULL, 204, 0, 'toogled'],
            [43, 'custom_baracks', 'Custom baracks', '', 'numberbox', 1, '0', 0, 50, NULL, 205, 0, 'toogled'],
            [44, 'custom_church', 'Custom church', '', 'numberbox', 1, '0', 0, 3, NULL, 206, 0, 'toogled'],
            [45, 'custom_smith', 'Custom smith', '', 'numberbox', 1, '0', 0, 50, NULL, 207, 0, 'toogled'],
            [46, 'custom_wood', 'Custom wood', '', 'numberbox', 1, '0', 0, 50, NULL, 208, 0, 'toogled'],
            [47, 'custom_stone', 'Custom stone', '', 'numberbox', 1, '0', 0, 50, NULL, 209, 0, 'toogled'],
            [48, 'custom_iron', 'Custom iron', '', 'numberbox', 1, '0', 0, 50, NULL, 210, 0, 'toogled'],
            [49, 'custom_market', 'Custom market', '', 'numberbox', 1, '0', 0, 50, NULL, 211, 0, 'toogled'],
            [50, 'custom_stable', 'Custom stable', '', 'numberbox', 1, '0', 0, 50, NULL, 212, 0, 'toogled'],
            [51, 'custom_wall', 'Custom wall', '', 'numberbox', 1, '0', 0, 50, NULL, 213, 0, 'toogled'],
            [52, 'custom_garage', 'Custom garage', '', 'numberbox', 1, '0', 0, 50, NULL, 214, 0, 'toogled'],
            [53, 'custom_hide', 'Custom hide', '', 'numberbox', 1, '0', 0, 50, NULL, 215, 0, 'toogled'],
            [54, 'custom_snob', 'Custom snob', '', 'numberbox', 1, '0', 0, 3, NULL, 216, 0, 'toogled'],
            [55, 'custom_statue', 'Custom statue', '', 'numberbox', 1, '0', 0, 1, NULL, 217, 0, 'toogled'],
            [56, 'custom_watchtower', 'Custom watchtower', '', 'numberbox', 1, '0', 0, 50, NULL, 218, 0, 'toogled'],
            [57, 'farmassistent', 'Farm assistent', '', 'checkbox', 1, '1', NULL, NULL, NULL, 35, 0, NULL],
            [58, 'accountmanager', 'Account manager', '', 'checkbox', 1, '0', NULL, NULL, NULL, 36, 0, NULL],
            [60, 'select_start', 'Select start positon after restart', '', 'checkbox', 1, '0', NULL, NULL, NULL, 400, 0, NULL],
            [61, 'restart', 'Restart after beeing noobled', '', 'checkbox', 1, '0', NULL, NULL, NULL, 410, 0, NULL],
            [38, 'win_cond', 'Win condition', '', 'selectbox', 1, '0', NULL, NULL,
                'none:None;one_player_max_points:Rank 1 player;one_player_max_off_rank:Rank 1 player by Off rank;one_player_max_deff_rank:Rank 1 player by Deff rank;one_player_max_villages:Player with maximum villages;one_tribe_max_points:Rank 1 tribe;one_tribe_max_off_rank:Rank 1 tribe by Off rank', 2, 0, NULL]
        );
        $Schema->getDB()->insertMulti("round_settings_controls", $multiInsertData, $dataKeys);
        $Schema->finishMigration(get_class($this));
    }

}
