<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202109201605_createMatchRounds
 *
 * @author jakub
 */
class migration_202109211604_createMatchRoundsSlots extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "match_rounds_slots");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "match_rounds_slots`   (
 `id` varchar(128) COLLATE utf8_bin NOT NULL,
 `round_id` int(11) NOT NULL,
 `type` varchar(16) COLLATE utf8_bin NOT NULL,
 `owner` int(11) NOT NULL,
 `map_id` int(11) DEFAULT NULL,
 `created` datetime NOT NULL DEFAULT current_timestamp(),
 `updated` datetime DEFAULT NULL ON UPDATE current_timestamp(),
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
        $Schema->rawTable(get_class($this), "match_rounds_slots", $raw);
    }

}
