<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202109201605_createMatchRounds
 *
 * @author jakub
 */
class migration_202109271700_createSettingsControls extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "round_settings_controls");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "round_settings_controls`   (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `internal_name` varchar(32) COLLATE utf8_bin NOT NULL,
 `name` varchar(64) COLLATE utf8_bin NOT NULL,
 `description` text COLLATE utf8_bin NOT NULL,
 `type` set('checkbox','numberbox','textbox','selectbox','textarea') COLLATE utf8_bin NOT NULL,
 `active` tinyint(1) NOT NULL DEFAULT '1',
 `default` varchar(16) COLLATE utf8_bin NOT NULL,
 `min` float DEFAULT NULL,
 `max` float DEFAULT NULL,
 `options` text COLLATE utf8_bin,
 `order_id` int(11) DEFAULT NULL,
 `hidden` tinyint(1) NOT NULL DEFAULT '0',
 `class` varchar(16) COLLATE utf8_bin DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
";
        $Schema->rawTable(get_class($this), "round_settings_controls", $raw);
    }

}
