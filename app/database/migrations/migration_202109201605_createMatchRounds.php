<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202109201605_createMatchRounds
 *
 * @author jakub
 */
class migration_202109201605_createMatchRounds extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "match_rounds");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "match_rounds`  (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `external_id` int(11) DEFAULT NULL,
 `time_start` datetime NOT NULL,
 `limit` int(11) NOT NULL DEFAULT 8,
 `enable_registrations` tinyint(1) NOT NULL DEFAULT 1,
  `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
 `updated` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
        $Schema->rawTable(get_class($this), "match_rounds", $raw);
    }

}
