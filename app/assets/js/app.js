
class App {
    constructor(server_url) {
        this.serverURL = server_url;
    }

    Init() {
        this.moveModals();
    }

    moveModals() {
        var modals = document.getElementsByClassName("modal");
        var pos = document.getElementById("modal_section");
        for (let index = 0, len = modals.length; index < len; ++index) {
            pos.appendChild(modals[index]);
        }
    }

    sendPOST(url, data) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);

//Send the proper header information along with the request
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function () { // Call a function when the state changes.
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                // Request finished. Do processing here.
            }
        };
        xhr.send(data);
    }
}