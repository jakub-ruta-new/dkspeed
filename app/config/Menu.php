<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\menu;

use JR\CORE\helpers\menu\MenuItem;

/**
 * Description of Menu
 *
 * @author jakub
 */
class Menu extends \JR\CORE\app\menu\AbstractMenu {

    public function AddToNavbar(): void {
        $rights = $this->user->rights;
        if ($rights->can("users", "view", false)) {
            $this->navbar->addOne(
                    new MenuItem("Home", "Placeholder",
                            "dashBoard", "fas fa-bullhorn"));
        }
        $this->navbar->addOne(
                new MenuItem("Home", "Reserve match on round",
                        "rounds/register", "fas fa-burn",
                        $this->request->startWith("rounds/register")));
        $this->navbar->addOne(
                new MenuItem("Home", "Plan new round",
                        "planner/new", "fas fa-plus",
                        $this->request->startWith("planner/new")));
    }

}
