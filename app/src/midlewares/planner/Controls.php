<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\midleware\planner;

/**
 * Description of Controls
 *
 * @author jakub
 */
class Controls extends \JR\CORE\midleware\Midleware {

    protected $primaryKey = "id";
    protected $tableName = "round_settings_controls";

    public function getActive() {
        return $this->db->where("active", 1)
                        ->orderBy("order_id", "ASC")
                        ->get($this->tableName);
    }

}
