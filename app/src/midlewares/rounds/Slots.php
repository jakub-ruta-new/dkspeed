<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\midlewares\rounds;

/**
 * Description of Slots
 *
 * @author jakub
 */
class Slots extends \JR\CORE\midleware\Midleware {

    protected $primaryKey = "id";
    protected $tableName = "match_rounds_slots";
    static $Slots = array("1v1" => "1v1");

    public function getCount($round_id) {
        return $this->db->where("round_id", $round_id)
                        ->getValue($this->tableName, "COUNT(*)");
    }

    public function takeSlot(array

            $form, array $round, int $user_id) {
        $this->db
                ->startTransaction();
        $this->db->setLockMethod("WRITE")->lock($this->tableName);
        if ($round['limit'] <= $this->getCount($round['id'])) {
            $this->db->unlock();
            throw new NoSlotAvaiableException();
        }
        $id = \JR\CORE\helpers\StringUtils::generate_string(64);
        $this->db->insert($this->tableName,
                ['id' => $id,
                    'type' => $form['type'],
                    'round_id' => $round['id'],
                    "owner" => $user_id]);
        $this->db->commit();
        return $id;
    }

}
