<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\midlewares\rounds;

/**
 * Description of Rounds
 *
 * @author jakub
 */
class Rounds extends \JR\CORE\midleware\Midleware {

    protected $primaryKey = "id";
    protected $tableName = "match_rounds";

    /**
     *
     * @var \JR\CORE\app\midlewares\rounds\Slots
     */
    public $slots;

    public function __construct(\MysqliDb $db) {
        parent::__construct($db);
        $this->slots = new Slots($db);
    }

    public function getActive() {
        return $this->db->where("time_start", date("Y-m-d H:i:s", time() + 60), ">")
                        ->get($this->tableName);
    }

}
