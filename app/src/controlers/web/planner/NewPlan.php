<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\controlers\web\planner;

/**
 * Description of NewPlan
 *
 * @author jakub
 */
class NewPlan extends \JR\CORE\controler\WebControler {

    public $view = 'app.planner.new';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = false;

    /**
     *
     * @var \JR\CORE\app\midleware\planner\Controls
     */
    protected $controls_utils;
    protected $util;

    public function execute() {
        $this->controls_utils = new \JR\CORE\app\midleware\planner\Controls($this->db);
        $this->createForm();
    }

    protected function createForm() {
        $controls = $this->controls_utils->getActive();
        $form = new \JR\CORE\helpers\form\FormFactory("plan_round", $this->dep->getSession()->getCSRFToken());
        $this->addControls($form, $controls);
        $form->createTextArea("description", "Additional info");
        //$form->createButton("send", "Send");
        $this->data['form_new_round'] = $form;
        bdump($form);
    }

    public function addControls(\JR\CORE\helpers\form\FormFactory $form, $controls) {
        foreach ($controls as $u) {
            if ($u['active']) {
                switch ($u['type']) {
                    case 'numberbox':
                        $this->addNumberBox($form, $u);
                        break;
                    case 'checkbox':
                        $this->addCheckBox($form, $u);
                        break;
                    case 'textbox':
                        $this->addTextBox($form, $u);
                        break;
                    case 'selectbox':
                        $this->addSelectBox($form, $u);
                        break;
                    default:
                        throw new \Exception("wrong type");
                }
            }
        }
    }

    public function addNumberBox(\JR\CORE\helpers\form\FormFactory $form, $u) {
        $c = $form->createNumberInput($u['internal_name'], $u['name'])
                ->setMin($u['min'])
                ->setMax($u['max'])
                ->value($u['default'])
                ->required();
        if (strlen($u['description']) > 1) {
            $c->setText($u['description']);
        }
    }

    public function addCheckBox(\JR\CORE\helpers\form\FormFactory $form, $u) {
        $c = $form->createCheckBox($u['internal_name'], $u['name'])
                ->value($u['default']);
        if (strlen($u['description']) > 1) {
            $c->setText($u['description']);
        }
    }

    public function addTextBox(\JR\CORE\helpers\form\FormFactory $form, $u) {
        $c = $form->createTextInput($u['internal_name'], $u['name'])
                ->value($u['default'])
                ->required();
        if (strlen($u['description']) > 1) {
            $c->setText($u['description']);
        }
    }

    public function addSelectBox(\JR\CORE\helpers\form\FormFactory $form, $u) {
        $options = array();
        foreach (explode(";", $u['options']) as $s) {
            $s1 = explode(":", $s);
            $options[$s1[0]] = $s1[1];
        }
        $c = $form->createSelect($u['internal_name'], $u['name'])
                ->value($u['default'])
                ->required()
                ->setOptions($options);

        if (strlen($u['description']) > 1) {
            $c->setText($u['description']);
        }
    }

}
