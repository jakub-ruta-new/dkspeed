<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\controlers\web\planner;

/**
 * Description of
 *
 * @author jakub
 */
class PlannerControler extends \JR\CORE\controler\WebControler {

    public $view = 'app.planner.menu';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = false;

    //put your code here
    public function execute() {
        switch ($this->request->getParsedPath()[1]) {
            case 'new':
                $new = new NewPlan($this->dep);
                break;
        }
        if (isset($new)) {
            $new->execute();
            $this->data = $new->data;
            $this->view = $new->view;
        }
    }

}
