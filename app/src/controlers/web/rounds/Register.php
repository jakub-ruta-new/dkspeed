<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\controlers\web\rounds;

/**
 * Description of Register
 *
 * @author jakub
 */
class Register extends \JR\CORE\controler\WebControler {

    public $view = 'app.rounds.register';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = false;

    /**
     *
     * @var \JR\CORE\app\midlewares\rounds\Rounds
     */
    protected $util;

    public function execute() {
        $this->util = new \JR\CORE\app\midlewares\rounds\Rounds($this->db);
        $this->prepareData();
    }

    public function prepareData() {
        $form_upcoming = array();
        $form_upcoming_disabled = array();
        bdump($this->util->getActive());
        foreach ($this->util->getActive() as $r) {
            $form_upcoming[$r['id']] = $r['time_start'];
            if (!$r['enable_registrations']) {
                $form_upcoming_disabled[] = $r['id'];
            }
        }
        $form = new \JR\CORE\helpers\form\FormFactory("register_for_round", $this->dep->getSession()->getCSRFToken());
        $form->createSelect("round", "Round start time")
                ->setOptions($form_upcoming)
                ->setDisabled($form_upcoming_disabled)
                ->required();
        $form->createSelect("type", "Match type")
                ->setOptions(["1v1" => "1v1", "2v2" => "2v2", "3v3",
                    "4v4" => "4v4", "5v5" => "5v5", "6v6" => "6v6",
                    "7v7" => "7v7", "8v8" => "8v8", "9v9" => "9v9",
                    "10v10" => "10v10"])
                ->value("1v1")
                ->required()
                ->readonly();
        $form->createButton("next", "Next");
        $this->data['form_reserve'] = $form;
        if ($form->isSend()) {
            $this->tryRegister($form);
            return;
        }
    }

    public function tryRegister(\JR\CORE\helpers\form\FormFactory $form) {
        $round_id = $form->getValues()['round'];
        $round = $this->util->getOne($round_id);
        try {
            $id = $this->util->slots->takeSlot($form->getValues(), $round, $this->dep->getUser()->getInternalId());
            $this->dep->getSession()
                    ->setMessages("Match slot on round has been registered!", "success");
            throw new \JR\CORE\router\RedirectError("rounds/edit/" . $id);
        } catch (\JR\CORE\app\midlewares\rounds\NoSlotAvaiableException $ex) {
            $this->noSpaceError();
        }
    }

    public function noSpaceError() {
        $this->dep->getSession()
                ->setMessages("We are sorry but there is no available slot in selected round", "danger");
        return false;
    }

}
