<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\controlers\web\rounds;

use \JR\CORE\router\RedirectToError;

/**
 * Description of Edit
 *
 * @author jakub
 */
class Edit extends \JR\CORE\controler\WebControler {

    public $view = 'app.rounds.edit';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = false;

    /**
     *
     * @var \JR\CORE\app\midlewares\rounds\Slots
     */
    protected $util;

    /**
     *
     * @var array of one record in db
     */
    protected $slot;

    /**
     *
     * @var array of one record in db
     */
    protected $round;

    public function execute() {
        $this->util = new \JR\CORE\app\midlewares\rounds\Slots($this->db);
        $this->checkSlotId();
        $this->prepareData();
    }

    protected function prepareData() {
        $form = new \JR\CORE\helpers\form\FormFactory("slot_edit", $this->dep->getSession()->getCSRFToken());
        $form->createTextInput("round_time", "Round time")
                ->readonly()
                ->value($this->round['time_start']);
        $form->createSelect("type", "Match type")
                ->readonly()
                ->required()
                ->setOptions(\JR\CORE\app\midlewares\rounds\Slots::$Slots)
                ->value($this->slot['type']);
        $this->data['form_round'] = $form;
    }

    protected function checkSlotId() {
        if (!isset($this->request->getParsedPath()[2])) {
            throw new RedirectToError(403, "You are not authorized to view requested slot!");
        }
        $this->slot = $this->util->getOne($this->request->getParsedPath()[2]);
        bdump($this->slot);
        if (!$this->dep->getUser()->rights->can("slots", "all", false) &&
                $this->slot['owner'] != $this->dep->getUser()->getInternalId()) {
            throw new RedirectToError(403, "You are not authorized to view/edit requested slot!");
        }
        $rounds_util = new \JR\CORE\app\midlewares\rounds\Rounds($this->db);
        $this->round = $rounds_util->getOne($this->slot['round_id']);
    }

}
