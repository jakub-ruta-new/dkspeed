<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\controlers\web\rounds;

/**
 * Description of RoundControler
 *
 * @author jakub
 */
class RoundControler extends \JR\CORE\controler\WebControler {

    public $view = 'app.rounds.menu';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = false;

    //put your code here
    public function execute() {
        switch ($this->request->getParsedPath()[1]) {
            case 'register':
                $new = new Register($this->dep);
                break;
            case 'edit':
                $new = new Edit($this->dep);
        }
        if (isset($new)) {
            $new->execute();
            $this->data = $new->data;
            $this->view = $new->view;
        }
    }

}
