<?php

require __DIR__ . '/vendor/autoload.php';

$cli = new \JR\CORE\cli\main\CLI_interface(__DIR__);
$cli->execute();
