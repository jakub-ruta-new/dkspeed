<?php

require 'vendor/autoload.php';

use Tracy\Debugger;
use JR\CORE\views;

session_start();
Debugger::enable(Debugger::DETECT, __DIR__ . '/app/temp/tracy');
Debugger::timer('app_start');
$app = new \JR\CORE\app\App();
$app->setENVFile(__DIR__);
$app->start();
